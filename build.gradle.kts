plugins {
    // this is necessary to avoid the plugins to be loaded multiple times
    // in each subproject's classloader
    alias(libs.plugins.jetbrainsCompose) apply false
    alias(libs.plugins.androidApplication) apply false
    alias(libs.plugins.androidLibrary) apply false
    alias(libs.plugins.kotlinMultiplatform) apply false
    alias(libs.plugins.complete.kotlin) apply false
    alias(libs.plugins.aboutLibraries) apply false
    alias(libs.plugins.moko.resources) apply false

}

buildscript {
    repositories {
        gradlePluginPortal()
    }
    dependencies {
        //classpath (libs.moko.classpath)
        classpath (libs.libres.classpath)
    }
}
plugins {
    alias(libs.plugins.kotlinMultiplatform)
    alias(libs.plugins.androidLibrary)
    alias(libs.plugins.moko.resources)
}

kotlin {
    listOf(
        iosX64(),
        iosArm64(),
        iosSimulatorArm64()
    ).forEach { iosTarget ->
        iosTarget.binaries.framework {
            baseName = "Shared"
            isStatic = true
        }
    }
    
    androidTarget {
        compilations.all {
            kotlinOptions {
                jvmTarget = libs.versions.java.get()
            }
        }
    }

    jvm()

    sourceSets {
        commonMain.dependencies {
            // put your Multiplatform dependencies here
            implementation(project.dependencies.platform(libs.kotlin.coroutines.bom))
            api(libs.moko.resources)
        }

        commonTest.dependencies {
            implementation(libs.kotlin.test)
            implementation(project.dependencies.platform(libs.kotlin.coroutines.bom))
            implementation(libs.moko.resources.test)
        }

        jvmMain {
            dependsOn(commonMain.get())
        }
    }
}

multiplatformResources {
    multiplatformResourcesPackage = "org.kvdevworks.gamesheets.shared"
}



android {
    namespace = "org.kvdevworks.gamesheets.shared"
    buildToolsVersion = libs.versions.android.buildTools.get()
    compileSdk = libs.versions.android.compileSdk.get().toInt()

    // hack for problem:
    // "Expected object 'MR' has no actual declaration in module <mpp-library_debug> for JVM"
    // Issue: https://github.com/icerockdev/moko-resources/issues/510
    // Example: https://gitlab.icerockdev.com/scl/boilerplate/mobile-moko-boilerplate/-/blob/master/mpp-library/build.gradle.kts#L146
    // Remove after fix this
    sourceSets {
        getByName("main").java.srcDirs("build/generated/moko/androidMain/src")
    }

    //sourceSets["main"].resources.exclude("src/commonMain/resources/MR")

    defaultConfig {
        minSdk = libs.versions.android.minSdk.get().toInt()
    }
    compileOptions {
        sourceCompatibility = JavaVersion.toVersion(libs.versions.java.get().toInt())
        targetCompatibility = JavaVersion.toVersion(libs.versions.java.get().toInt())
    }
}

package quixx

import dev.icerock.moko.resources.StringResource
import org.kvdevworks.gamesheets.shared.MR


fun getRowNumbersAscending(): Array<StringResource> {
    return arrayOf(
        MR.strings.quixx_board_value_digit_two,
        MR.strings.quixx_board_value_digit_three,
        MR.strings.quixx_board_value_digit_four,
        MR.strings.quixx_board_value_digit_five,
        MR.strings.quixx_board_value_digit_six,
        MR.strings.quixx_board_value_digit_seven,
        MR.strings.quixx_board_value_digit_eight,
        MR.strings.quixx_board_value_digit_nine,
        MR.strings.quixx_board_value_digit_ten,
        MR.strings.quixx_board_value_digit_eleven,
        MR.strings.quixx_board_value_digit_twelve,
    )
}
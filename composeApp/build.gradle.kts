import org.jetbrains.compose.ExperimentalComposeLibrary
import org.jetbrains.compose.desktop.application.dsl.TargetFormat

plugins {
    alias(libs.plugins.kotlinMultiplatform)
    alias(libs.plugins.androidApplication)
    alias(libs.plugins.jetbrainsCompose)
    alias(libs.plugins.kotlinKsp)
    alias(libs.plugins.aboutLibraries)
    alias(libs.plugins.moko.resources)
}

kotlin {
    androidTarget {
        compilations.all {
            kotlinOptions {
                jvmTarget = libs.versions.java.get()
            }
        }
    }
    
    jvm("desktop")
    
    sourceSets {
        val desktopMain by getting {
            dependsOn(commonMain.get())
        }

        androidMain {
            dependsOn(commonMain.get())
        }

        androidMain.dependencies {
            implementation(libs.androidx.compose.ui)
            implementation(libs.androidx.activity.compose)
            implementation(project.dependencies.platform(libs.androidx.compose.bom))
            implementation(project.dependencies.platform(libs.kotlin.coroutines.bom))
        }

        desktopMain.dependencies {
            implementation(compose.desktop.currentOs)
        }

        commonMain.dependencies {
            implementation(projects.shared)
            implementation(compose.runtime)
            implementation(compose.foundation)
            implementation(compose.material)
            implementation(compose.materialIconsExtended)
            @OptIn(ExperimentalComposeLibrary::class)
            implementation(compose.components.resources)
            implementation(libs.kotlin.dateTime)
            implementation(project.dependencies.platform(libs.kotlin.coroutines.bom))
            implementation(libs.compose.aspectRatio)
            implementation(compose.uiTooling)
            implementation(libs.bundles.moko.resources)
            implementation (libs.aboutLibraries.compose)
        }

        commonTest.dependencies {
            implementation(libs.bundles.kotlin.testing.jvm)
            implementation(project.dependencies.platform(libs.kotlin.coroutines.bom))
        }

        androidNativeTest.dependencies {
            implementation(project.dependencies.platform(libs.androidx.compose.bom))
        }

    }
}


multiplatformResources {
    multiplatformResourcesPackage = "org.kvdevworks.gamesheets.composeApp"
}



android {
    namespace = "org.kvdevworks.gamesheets"
    buildToolsVersion = libs.versions.android.buildTools.get()
    compileSdk = libs.versions.android.compileSdk.get().toInt()

    sourceSets["main"].manifest.srcFile("src/androidMain/AndroidManifest.xml")
    sourceSets["main"].res.srcDirs("src/androidMain/res")
    sourceSets["main"].resources.srcDirs("src/commonMain/resources")

    // hack for problem:
    // "Expected object 'MR' has no actual declaration in module <mpp-library_debug> for JVM"
    // Issue: https://github.com/icerockdev/moko-resources/issues/510
    // Example: https://gitlab.icerockdev.com/scl/boilerplate/mobile-moko-boilerplate/-/blob/master/mpp-library/build.gradle.kts#L146
    // Remove after fix this
    sourceSets {
        getByName("main").java.srcDirs("build/generated/moko/androidMain/src")
    }

    defaultConfig {
        applicationId = "org.kvdevworks.gamesheets"
        minSdk = libs.versions.android.minSdk.get().toInt()
        targetSdk = libs.versions.android.targetSdk.get().toInt()
        versionCode = 1
        versionName = "1.0"
    }
    buildFeatures {
        compose = true
    }
    composeOptions {
        kotlinCompilerExtensionVersion = libs.versions.compose.compiler.get()
    }
    packaging {
        resources {
            excludes += "/META-INF/{AL2.0,LGPL2.1}"
        }
    }
    buildTypes {
        getByName("release") {
            isMinifyEnabled = false
        }
    }
    compileOptions {
        sourceCompatibility = JavaVersion.toVersion(libs.versions.java.get().toInt())
        targetCompatibility = JavaVersion.toVersion(libs.versions.java.get().toInt())
    }

    kotlin {
        jvmToolchain {
            languageVersion = JavaLanguageVersion.of(libs.versions.java.get().toInt())
        }
    }

    dependencies {
        debugImplementation(libs.androidx.compose.ui.tooling)
    }
}

compose.desktop {
    application {
        mainClass = "MainKt"

        nativeDistributions {
            targetFormats(TargetFormat.Dmg, TargetFormat.Msi, TargetFormat.Deb)
            packageName = "org.kvdevworks.gamesheets"
            packageVersion = "1.0.0"
        }

        dependencies {
            debugImplementation(compose.uiTooling)
            debugImplementation(libs.compose.desktop.previewer)
        }
    }
}

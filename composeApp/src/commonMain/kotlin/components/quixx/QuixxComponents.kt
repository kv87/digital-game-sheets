package components.quixx

import androidx.compose.foundation.BorderStroke
import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.aspectRatio
import androidx.compose.foundation.layout.defaultMinSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.layout.wrapContentHeight
import androidx.compose.foundation.layout.wrapContentSize
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.ButtonDefaults
import androidx.compose.material.Icon
import androidx.compose.material.IconToggleButton
import androidx.compose.material.OutlinedButton
import androidx.compose.material.Text
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Lock
import androidx.compose.material.icons.filled.LockOpen
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import dev.icerock.moko.resources.compose.stringResource
import quixx.getRowNumbersAscending

@Composable
fun QuixxNumber(textState: String,
                color: Color,
                modifier: Modifier = Modifier) {

    val number by remember { mutableStateOf(textState) }
    var isSelected by remember { mutableStateOf(false) }

    OutlinedButton(
        onClick = {
            isSelected = !isSelected
        },
        colors = ButtonDefaults.outlinedButtonColors(backgroundColor = Color.Black, contentColor = Color.Cyan),
        border = BorderStroke(width = 2.dp, color = Color.LightGray),
        shape = RoundedCornerShape(4.dp),
        modifier = modifier.aspectRatio(1f, true)

    ) {
        val text = if (isSelected) {
            "X"
        } else {
            number
        }
        Text(
            text = text,
            fontSize = 18.sp,
            color = color,
            fontWeight = FontWeight.Bold,
            textAlign = TextAlign.Center,
            softWrap = false,
            maxLines = 1
        )
    }
}

@Composable
fun QuixxLock(color: Color, modifier: Modifier = Modifier) {

    var checked by remember { mutableStateOf(false) }

    IconToggleButton(
        checked = checked,
        onCheckedChange = {
            checked = !checked

        },
        modifier = modifier
            .wrapContentSize()
            .width(48.dp)
            .height(48.dp)
            .background(color = Color.Black, shape = CircleShape)
            .border(
                width = 2.dp, color = Color.LightGray, shape = CircleShape
            ),
    ) {
        Icon(
            imageVector = if (checked) Icons.Default.Lock else Icons.Default.LockOpen,
            contentDescription = if (checked) "Row Locked" else "Row Unlocked",
            tint = color,
            modifier = Modifier.size(18.dp)
        )
    }
}

@Composable
fun QuixxRow(color: Color, reverseNumbers: Boolean = false) {
    Row(
        modifier = Modifier.background(color)
            .fillMaxWidth()
            .defaultMinSize(minHeight = 48.dp)
            .wrapContentHeight()
            .padding(8.dp),
        horizontalArrangement = Arrangement.spacedBy(16.dp),
        verticalAlignment = Alignment.CenterVertically
    ) {

        val numbers = getRowNumbersAscending()

        if (reverseNumbers) {
            numbers.reversedArray().forEach { number ->

                QuixxNumber(stringResource(number),
                    color = color,
                    modifier = Modifier.weight(1f))
            }
        } else {
            numbers.forEach { number ->
                QuixxNumber(stringResource(number),
                    color = color,
                    modifier = Modifier.weight(1f))
            }
        }

        QuixxLock(color, modifier = Modifier.weight(1f))
    }
}

@Composable
fun QuixxRowGroup() {
    val maxRows = 4
    Column(
        modifier = Modifier.wrapContentHeight()
            .fillMaxWidth()
    ) {
        for (i in 1..maxRows) {
            val color = when (i) {
                1 -> Color.Red
                2 -> Color.Yellow
                3 -> Color.Green
                4 -> Color(0xff91a4fc)
                else -> Color.Gray
            }

            val reverse = i > 2
            QuixxRow(color, reverse)
        }
    }
}